import { Injectable } from '@angular/core';

import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { BrowserWindowService } from '@ngcore/core';

import { GlobalThemeHelper } from '../helpers/global-theme-helper';


/**
 * UI helper methods for GlobalThemeHelper.
 */
@Injectable({
  providedIn : 'root'
})
export class GlobalThemeHandler {

  constructor(
    private browserWindowService: BrowserWindowService,
    private globalThemeHelper: GlobalThemeHelper,
  ) {
  }


  get hasSavedTheme(): boolean {
    return this.globalThemeHelper.hasDefaultTheme();
  }

  handleSaveCurrentTheme() {
    if(isDL()) dl.log("'Save Current' button clicked.");
    this.globalThemeHelper.saveDefaultTheme();
  }

  // Use random theme (from now on, not just once)
  handleUseRandomTheme() {
    if(isDL()) dl.log("'Use Random' button clicked.");
    this._useRandomTheme();
  }
  private _useRandomTheme() {
    if (this.browserWindowService.document) {
      this.globalThemeHelper.removeDefaultTheme();
      let theme = this.globalThemeHelper.getDifferentTheme();
      // this.document.getElementById('global-theme').setAttribute('href', theme);
      this.browserWindowService.document.getElementById('global-theme').setAttribute('href', theme);
    } else {
      if(isDL()) dl.log("browserWindowService.document is null.");
    }
  }

  handleTryDifferentThemes() {
    if(isDL()) dl.log("'Try Different' button clicked.");
    this._tryDifferentTheme();
  }
  private _tryDifferentTheme() {
    if (this.browserWindowService.document) {
      let theme = this.globalThemeHelper.getDifferentTheme();
      // this.document.getElementById('global-theme').setAttribute('href', theme);
      this.browserWindowService.document.getElementById('global-theme').setAttribute('href', theme);
    } else {
      if(isDL()) dl.log("browserWindowService.document is null.");
    }
  }


}
