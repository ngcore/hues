import { NgModule, ModuleWithProviders, ErrorHandler } from '@angular/core';
import { CommonModule } from '@angular/common';

import { NgCoreCoreModule } from '@ngcore/core';

import { GlobalThemeHelper } from './helpers/global-theme-helper';
import { GlobalThemeHandler } from './handlers/global-theme-handler';
import { CommonHuesService } from './services/common-hues-service';

@NgModule({
  imports: [
    CommonModule,
    NgCoreCoreModule.forRoot(),
  ],
  declarations: [
  ],
  exports: [
  ],
  entryComponents: [
    // ????
  ]
})
export class NgCoreHuesModule {
  static forRoot(): ModuleWithProviders<NgCoreHuesModule> {
    return {
      ngModule: NgCoreHuesModule,
      providers: [
        GlobalThemeHelper,
        GlobalThemeHandler,
        CommonHuesService
      ]
    };
  }
}
