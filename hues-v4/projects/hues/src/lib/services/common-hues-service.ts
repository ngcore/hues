import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import { DevLogger as dl } from '@ngcore/core'; import isDL = dl.isLoggable;
import { DateTimeUtil, DateIdUtil } from '@ngcore/core';


// Placeholder
@Injectable({
  providedIn : 'root'
})
export class CommonHuesService {

  constructor(
  ) {
    // if(isDL()) dl.log('Hello CommonHuesService Provider');
  }

  dummyFunction() {
  }

  // tbd:
  // ...

}
