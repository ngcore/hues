# @ngcore/hues
> NG Core angular/typescript UI helper library


UI helper library for "Look and feel" and color/hues, etc.
(Current version requires Angular v7.2+)


## API Docs

* http://ngcore.gitlab.io/hues/




